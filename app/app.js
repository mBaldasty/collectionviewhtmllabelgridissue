import Vue from 'nativescript-vue'
import DrawerPlugin from '@nativescript-community/ui-drawer/vue'
Vue.use(DrawerPlugin);
import { install } from '@nativescript-community/ui-drawer';
install();
import CollectionView from '@nativescript-community/ui-collectionview/vue';
Vue.use(CollectionView);
import Home from './components/Home'
require('@nativescript-community/ui-label').enableIOSDTCoreText();
Vue.registerElement('HTMLLabel', () => require('@nativescript-community/ui-label').Label);
import * as imageModule from '@nativescript-community/ui-image'
import ImagePlugin from "@nativescript-community/ui-image/vue";
Vue.use(ImagePlugin)
imageModule.initialize({ isDownsampleEnabled: true })
import PBSPlugin from '@nativescript-community/ui-persistent-bottomsheet/vue';
import {install as pbsInstall } from '@nativescript-community/ui-persistent-bottomsheet'
pbsInstall();
Vue.use(PBSPlugin)
Vue.config.silent = false
import Sample3 from "~/components/Sample3";
new Vue({
  render: (h) => h('frame', [h(Home)]),
}).$start()
